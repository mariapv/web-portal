<?php
$title="Home"; 
require("includes/header.php"); 
?>

  <body class="home">

	<?php require("includes/navigation.php"); ?>

    <!-- Begin page content -->
    <div class="container">

    	<div class="panel panel-default" id="login-container">

    		<div class="panel-heading">
				<h4>User Login</h4>
			</div>

			<div class="panel-body">

		      <form class="login-form">

		        <label for="inputUsername">Username</label>
		        <input type="username" id="inputUsername" class="form-control" required autofocus>
		        <label for="inputPassword">Password</label>
		        <input type="password" id="inputPassword" class="form-control" required>

		        <button class="btn btn-lg btn-default btn-block" type="submit">Login</button>

		      </form>

		    </div>

	    </div>

    </div> <!-- /container -->


    <?php require("includes/footer.php"); ?>
    
  </body>
</html>