    <footer class="footer">
      <div class="container">
        <p class="text-muted text-right col-md-6">Macquarie University Virtual Museums Portal</p>
		<p class="text-muted text-left col-md-6">Designed and created by PACE Group 7 S1, 2017</p>
      </div>
    </footer>

    
        <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <!-- DataTables Plugin JavaScript -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

