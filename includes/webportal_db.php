<?php
// database connection using PHP::PDO ***
$dbloc = "local"; // Use "local", "mq" or add others as appropriate
if ($dbloc == "mq") {
    $dbhost = 'ash.science.mq.edu.au';
    $dbusername = '2017g7';
    $dbuserpassword = 'p9RS2Sn5v5PA';
} else if ($dbloc == "local") {
    $dbhost = 'localhost';
    $dbusername = 'root';
    $dbuserpassword = 'root';
    $default_dbname = 'ARTS_COLLECTION';
} else if ($dbloc == 'sp2') {
    $dbhost = 'localhost';
    $dbusername = 'pace';
    $dbuserpassword = 'H@ppyB1rthday';
    // *********** need to setup local db ***********
    $default_dbname = 'ARTS_COLLECTION';  
}

// Use PDO to connect to the database; return the PDO object
function db_connect() {
    global $dbloc, $dbhost, $dbusername, $dbuserpassword, $default_dbname, $oraDB, $sid;
    // Set a default exception handler, so that we don't spill our guts if a query fails.
    set_exception_handler("exception_handler");

    // Remote Connection
    if ($dbloc == "mq") {
        $db = new PDO("mysql:host=$dbhost;charset=utf8", $dbusername, $dbuserpassword);
    }
    else {
        // Local Connection
        $db = new PDO("mysql:host=$dbhost;dbname=$default_dbname;charset=utf8", $dbusername, $dbuserpassword);
    }

    $db->setAttribute(PDO::ATTR_PERSISTENT, true);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    return $db;
}

function exception_handler(RuntimeException $ex) {
    $debug = true;		// If true, report to screen; otherwise silently log and die.
    if(get_class($ex) == "PDOException") {
        if ($debug == true)
            echo "PDO Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ":<br/>Code " . $ex->getCode() . " - " . $ex->getMessage();
        else
            error_log("PDO Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ": Code " . $ex->getCode() . " - " . $ex->getMessage());
    }
    else {
        error_log("Unhandled Exception in file " . basename($ex->getFile()) . ", line " . $ex->getLine() . ": Code " . $ex->getCode() . " - " . $ex->getMessage());
        // Any other unhandled exceptions will wind up at the home page, for safety
        header("Location: index.php");
    }
}
?>