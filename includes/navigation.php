<div class="container">

      <header>
          <img src="images/MacquarieLogo.png" id="logo" />
      </header>

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Virtual Museums Portal</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php" class="home">Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle item" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Items<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="item_list.php">Item List</a></li>
                  <li><a href="add_items.php">Add Items</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle collection" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Collections<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="create_collection.php">Create Collection</a></li>
                  <li><a href="view_collections.php">View Collections</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle exhibit" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Exhibits<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="create_exhibit.php">Create Exhibit</a></li>
                  <li><a href="view_exhibits.php">View Exhibits</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

 </div> <!-- /container -->