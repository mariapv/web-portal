<?php
/* includes database */
require_once("webportal_db.php");

# connect to db 
$db = db_connect();

/* attempt to query database or die function */
function try_or_die($sql) {
	try {
		$sql->execute();
	} catch (PDOException $ex) {
		echo $ex->getMessage();
		die ("Invalid Query");
	}
}

/* Retrieve artefacs from database */
function getArtefacts($db) {
	$sql = $db->prepare("SELECT * FROM ARTEFACTS");

	try_or_die($sql);

	while($row = $sql->fetch(PDO::FETCH_ASSOC)) {
		$rows[] = $row;
	}

	return $rows;
}



?>