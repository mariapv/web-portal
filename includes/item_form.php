<?php
/* includes database */
require_once("webportal_db.php");

# connect to db 
$db = db_connect();

/* attempt to query database or die function */
function try_or_die($sql) {
	try {
		$sql->execute();
	} catch (PDOException $ex) {
		echo $ex->getMessage();
		die ("Invalid Query");
	}
}

/*-------------------- Add Item To Database ----------------------*/

//if (isset($_POST['saveItem'])) {

		// keep track of post values

		// Artefacts Table
		$title = $_POST['title'];
		$thumbnail = $_POST['thumbnail'];
		$artID = $_POST['artID'];

		// Artefact_supp_data
		$accessionNum = $_POST['accessionNum'];
		$creator = $_POST['creator'];
		$culture = $_POST['culture'];
		$artDate = $_POST['artDate'];
		$artPeriod = $_POST['period'];
		$provenance = $_POST['provenance'];
		$materials = $_POST['materials'];
		$dimensions = $_POST['dimensions'];
		$artCol = $_POST['artCol'];
		$artSource = $_POST['artSource'];
		$classification = $_POST['classification'];
		$preservation = $_POST['preservation'];
		$narrative = $_POST['narrative'];

		// Artefact_ed_res
		$edRes = $_POST['edRes'];

		// Artefact_ol_res table
		$onlineRes = $_POST['onlineRes'];

		// Artefact_bibliography Table
		$furtherRead = $_POST['furtherRead'];
		$pubRef = $_POST['pubRef'];

		// Artefact_related_obj table
		$intRelObj = $_POST['intRelObj'];
		$extRelObj = $_POST['extRelObj'];
		$relObjLink = $_POST['relObjLink'];
		$parallels = $_POST['parallels']; 

		//insert into artefacts table
		$sql = $db->prepare("INSERT INTO ARTEFACTS(ART_ID_PK, ART_TITLE, ART_THUMB) VALUES(:ID, :TITLE, :THUMBNAIL)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':TITLE', $title);
		$sql->bindparam(':THUMBNAIL', $thumbnail);

		//try_or_die($sql); 

		//insert into artefact_supp_data table
		$sql = $db->prepare("INSERT INTO ARTEFACT_SUPP_DATA(ART_SD_ID_FK, ART_ACCESSION_NUM, ART_CREATOR, ART_CULTURE, ART_PERIOD, ART_DATE, ART_PROVENANCE, ART_MATERIALS, ART_DIMENSIONS, ART_COLLECTION, ART_SOURCE, ART_CLASSIFICATION, ART_PRESERVATION, ART_NARRATIVE) VALUES(:ID, :ACCESSIONNUM, :CREATOR, :CULTURE, :PERIOD, :ARTDATE, :PROVENANCE, :MATERIALS, :DIMENSIONS, :ARTCOL, :ARTSOURCE, :CLASSIFICATION, :PRESERVATION, :NARRATIVE)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':ACCESSIONNUM', $accessionNum);
		$sql->bindparam(':CREATOR', $creator);
		$sql->bindparam(':CULTURE', $culture);
		$sql->bindparam(':PERIOD', $artPeriod);
		$sql->bindparam(':ARTDATE', $artDate);
		$sql->bindparam(':PROVENANCE', $provenance);
		$sql->bindparam(':MATERIALS', $materials);
		$sql->bindparam(':DIMENSIONS', $dimensions);
		$sql->bindparam(':ARTCOL', $artCol);
		$sql->bindparam(':ARTSOURCE', $artSource);
		$sql->bindparam(':CLASSIFICATION', $classification);
		$sql->bindparam(':PRESERVATION', $preservation);
		$sql->bindparam(':NARRATIVE', $narrative);

		//try_or_die($sql); 

		//insert into artefact_ed_res table
		$sql = $db->prepare("INSERT INTO ARTEFACT_ED_RES(ART_ER_ID_FK, ART_ED_RES_FILE_LOC) VALUES(:ID, :ED_RES)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':ED_RES', $edRes);

		//try_or_die($sql); 

		//insert into artefact_ol_res table
		$sql = $db->prepare("INSERT INTO ARTEFACT_OL_RES(ART_OLR_ID_FK, ART_OL_RES_LNK) VALUES(:ID, :OL_RES)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':OL_RES', $onlineRes);

		//try_or_die($sql); 

		//insert into artefact_bibliography table
		$sql = $db->prepare("INSERT INTO ARTEFACT_BIBLIOGRAPHY(ART_BIB_ID_FK, ART_BIB_FURTHER_READ, ART_BIB_REF) VALUES(:ID, :FURTHER_READ, :PUB_REF)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':FURTHER_READ', $furtherRead);
		$sql->bindparam(':PUB_REF', $pubRef);

		//try_or_die($sql); 

		//insert into artefact_related_obj table
		$sql = $db->prepare("INSERT INTO ARTEFACT_REL_OBJ(ART_RO_ID_FK, ART_RO_INT_ID, ART_RO_EXT_ID, ART_RO_EXT_LINK) VALUES(:ID, :INT_REL, :EXT_REL, :LINK)");

		$sql->bindparam(':ID', $artID);
		$sql->bindparam(':INT_REL', $intRelObj);
		$sql->bindparam(':EXT_REL', $extRelObj);
		$sql->bindparam(':LINK', $relObjLink);

		try_or_die($sql); 

		header("Location: ../add_items.php")
	
//}

?>