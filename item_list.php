<?php
$title="Items"; 
include("includes/header.php"); 
require("includes/controller.php");
?>
	<body class="item">

		<?php include("includes/navigation.php"); ?>

            <div class="container">

      		<div class="panel panel-default">
      			<!-- Search Box -->
      			<div class="panel-heading">
      				<div class="col-md-2">
            				<h4>Search Items</h4>
            			</div>
            			<div class="input-group col-md-10">
            				<!-- Search Bar and Button -->
            				<input type="search" placeholder="Enter Keyword" class="form-control" aria-describedby="basic-addon2" id="searchTable">
            				<span class="input-group-addon" id="basic-addon2">
            					<button type="submit">
            						<span class="glyphicon glyphicon-search"></span>
            					</button>
            				</span>
            			</div>
            		</div>
            		<div class="panel-body">
            			<!-- Table that displays artefacts from database -->
                              <!-- Table functionality achieved with DataTables Plugin -->
            			<table id="artefactsTable" class="table table-bordered table-hover">
            			<!-- Table Headings -->
            				<thead>
            				<tr>
            					<th>Artefact Name</th>
            					<th>Artefact Description</th>
            					<th>Edit</th>
            				</tr>
            				</thead>
            				<tbody>
                                    <!-- Fill Table rows with data from database, call the getArtefacts function in Controller.php -->
            				<?php
            					$rows = getArtefacts($db);
            					foreach($rows as $row):
            						echo '<tr><td>'.$row['ART_TITLE'].'</td><td>'.$row['ART_DESC'].'</td>';
            						echo '<td style="text-align: center;">
            								<button type="button" id="editButton">
       										 <span class="glyphicon glyphicon-edit"></span>
      									</button>
      								 </td></tr>';
            					endforeach;
            				?>
            				</tbody>
            			</table>
      			</div>
      		</div>
            </div>

		<?php include("includes/footer.php"); ?>

		<script>
			$(document).ready(function(){
				// Call DataTables plugin to sort table and paginate
				var dataTable = $('#artefactsTable').DataTable({
					"dom": "ltipr",
					"order": [[0, 'asc']],
					"lengthMenu": [5, 15, 25, 50, 100],
					"pageLength": 15,
                              "columnDefs": [{
                                    "orderable": false,
                                    "targets": 2
                              }]
				});

				// Search and Filter function
				$('#searchTable').keyup(function(){
					dataTable.search(this.value).draw();
				});
			});
		</script>

	</body>
</html>