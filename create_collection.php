<?php
$title="Create Collection"; 
include("includes/header.php"); 
require("includes/controller.php");
?>
	<body class="collection">

		<?php include("includes/navigation.php"); ?>

		<div class="container">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4>Create/Edit Collection</h4>
				</div>

				<div class="panel-body">
					<div class="col-md-12">
						<div class="input-group">
		      				<!-- Search Bar and Button -->
		      				<input type="search" placeholder="Enter Keyword" class="form-control" aria-describedby="basic-addon2" id="searchColTable">
		      				<span class="input-group-addon" id="basic-addon2">
		      					<button type="submit">
		      						<span class="glyphicon glyphicon-search"></span>
		      					</button>
		      				</span>
		      			</div>

		      			<div id="colTableContainer">
							<!-- Table that displays artefacts from database -->
			      			<table id="colTable" class="table table-bordered">
			      			<!-- Table Headings -->
			      				<thead>
			      				<tr>
			      					<th> Select </th>
			      					<th>Artefact Name</th>
			      					<th>Artefact Description</th>
			      				</tr>
			      				</thead>
			      				<tbody>
			      				<!-- Table Rows from Database -->
			      				<?php
			      					$rows = getArtefacts($db);
			      					foreach($rows as $row):
			      						echo '<tr><td style="text-align: center;><div class="checkbox"><input type="checkbox" value="'.$row['ART_ID_PK'].'"></div></td>';
			      						echo '<td>'.$row['ART_TITLE'].'</td><td>'.$row['ART_DESC'].'</td></tr>';
			      					endforeach;
			      				?>
			      				</tbody>
			      			</table>
		      			</div>
	      			</div>

	      			<!-- Add Collection Details Form -->
					<form action="/controller.php" method="post" id="col_form">
						<div class="form-group">
							<label for="col_name">Collection Name</label>
							<input type="text" class="form-control" id="col_name">
						</div>
						<div class="form-group">
							<label for="col_desc">Collection Description</label>
							<textarea class="form-control" id="col_desc" rows="4"></textarea>
						</div>
						<div class="form-group">
							<label for="art_thumbnail">Upload Thumbnail<span class="required-fields"> *</span></label>
							<input type="file" id="art_thumbnail" multiple>
						</div>
						<button type="submit" class="btn btn-default">Create Collection</button>
					</form>

				</div>

			</div>
		</div>	

		<?php include("includes/footer.php"); ?>

		<script>
			$(document).ready(function(){
				// Call DataTables plugin to sort table and paginate
				var dataTable = $('#colTable').DataTable({
					"dom": "tr",
					"order": [[1, 'asc']],
					"pageLength": 3,
					"columnDefs": [{
						"orderable": false,
						"targets": 0
					}]
				});

				// Search and Filter function
				$('#searchColTable').keyup(function(){
					dataTable.search(this.value).draw();
				});


				// Highlight checked table row
				$('#colTable input:checkbox').click(function(){
					$(this).closest('tr').toggleClass("selected");
				});

			});
		</script>

	</body>
</html>