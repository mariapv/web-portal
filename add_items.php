<?php
$title="Add Items"; 
include("includes/header.php"); 
?>
	<body class="item">

		<?php include("includes/navigation.php"); ?>

		<div class="container">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4>Add/Edit Item</h4>
				</div>

				<div class="panel-body">

					<p class="required-fields">* Required Fields</p>

					<form action="includes/item_form.php" method="post" id="art_form">
						<fieldset>
						<legend>Introductory Text</legend>
							<div class="form-group col-md-6">
								<label for="title">Title<span class="required-fields"> *</span></label>
								<input type="text" class="form-control" name="title" size="5" required>
							</div>
							<div class="form-group col-md-6">
								<label for="accessionNum">Accession Number<span class="required-fields"> *</span></label>
								<input type="text" class="form-control" name="accessionNum" size="5" required>
							</div>
							<div class="form-group col-md-6">
								<label for="artID">Item ID</label>
								<input type="text" class="form-control" name="artID">
							</div>
							<div class="form-group col-md-6">
								<label for="creator">Creator</label>
								<input type="text" class="form-control" name="creator">
							</div>
							<div class="form-group col-md-6">
								<label for="culture">Culture</label>
								<input type="text" class="form-control" name="culture">
							</div>
							<div class="form-group col-md-6">
								<label for="period">Period</label>
								<input type="text" class="form-control" name="period">
							</div>
							<div class="form-group col-md-6">
								<label for="artDate">Date</label>
								<input type="text" class="form-control" name="artDate">
							</div>
							<div class="form-group col-md-6">
								<label for="provenance">Provenance</label>
								<input type="text" class="form-control" name="provenance">
							</div>
							<div class="form-group col-md-6">
								<label for="materials">Materials</label>
								<input type="text" class="form-control" name="materials">
							</div>
							<div class="form-group col-md-6">
								<label for="dimensions">Dimensions</label>
								<input type="text" class="form-control" name="dimensions">
							</div>
							<div class="form-group col-md-6">
								<label for="artCol">Collection</label>
								<input type="text" class="form-control" name="artCol">
							</div>
							<div class="form-group col-md-6">
								<label for="artSource">Source</label>
								<input type="text" class="form-control" name="artSource">
							</div>
							<div class="form-group col-md-6">
								<label for="thumbnail">Upload Thumbnail<span class="required-fields"> *</span></label>
								<input type="file" name="thumbnail">
							</div>
						</fieldset>
						<fieldset>
							<legend>Information Tab</legend>
							<div class="form-group col-md-6">
								<label for="classification">Classification</label>
								<input type="text" class="form-control" name="classification">
							</div>
							<div class="form-group col-md-6">
								<label for="preservation">Preservation</label>
								<input type="text" class="form-control" name="preservation">
							</div>
							<div class="form-group col-md-12">
								<label for="narrative">Narrative<span class="required-fields"> *</span></label>
								<textarea class="form-control" name="narrative" rows="4" required></textarea>
							</div>
						</fieldset>
						<fieldset>
							<legend>Resources Tab</legend>
							<div class="form-group col-md-12">
								<label for="edRes">Educational Resources</label>
								<textarea class="form-control" name="edRes" rows="5"></textarea>
								<input type="file" id="edUpload" multiple>
							</div>
							<div class="form-group col-md-12">
								<label for="onlineRes">Online Resources</label>
								<textarea class="form-control" name="onlineRes" rows="5"></textarea>
							</div>
							<h4>Bibliography/References</h4>
							<div class="form-group col-md-12">
								<label for="furtherRead">Further Reading</label>
								<textarea class="form-control" name="furtherRead" rows="3"></textarea>
							</div>
							<div class="form-group col-md-12">
								<label for="pubRef">Published References</label>
								<textarea class="form-control" name="pubRef" rows="3"></textarea>
							</div>
						</fieldset>
						<fieldset>
							<legend>Related Material Tab</legend>
							<h4>Related Objects</h4>
							<div class="form-group col-md-6">
								<label for="intRelObj">Internal</label>
								<input class="form-control" name="intRelObj"></input>
							</div>
							<div class="form-group col-md-6">
								<label for="extRelObj">External</label>
								<input class="form-control" name="extRelObj"></input>
							</div>
							<div class="form-group col-md-12">
								<label for="relObjLink">Link</label>
								<input class="form-control" name="relObjLink"></input>
							</div>
						</fieldset>
						<br>
						<fieldset>
							<div class="form-group col-md-12">
								<label for="parallels">Parallels</label>
								<textarea class="form-control" name="parallels" rows="3"></textarea>
							</div>
						</fieldset>
						<fieldset>
							<legend>Make Public</legend>
							<div class="form-group col-md-6">
								<label for="publish">Publish</label>
								<input type="checkbox" name="publish">
							</div>
							<div class="form-group col-md-6">
								<label for="unpublish">Unpublish</label>
								<input type="checkbox" name="unpublish">
							</div>
						</fieldset>
						<fieldset>
							<button type="submit" name="saveItem" class="btn btn-success">Save</button>
							<button type="reset" class="btn btn-danger">Delete Record</button>
							<button type="reset" class="btn btn-default">Clear All</button>
						</fieldset>	
					</form>

				</div>

			</div>
		</div>	

		<?php include("includes/footer.php"); ?>


	</body>
</html>